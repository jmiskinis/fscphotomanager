"""
Parses a given JSON file, downloads the photos and saves them in the database.
"""

import argparse
import json
import errno
import os
import random
import string
import sqlite3
import urllib.request

from PIL import Image
from manager.settings import MEDIA_ROOT

# Ideally, these two should be defined dynamically. However, UPLOAD_TO is
# inaccessible from outside of Django's REST Framework.
# pylint: disable=no-member
UPLOAD_TO = "photos/"
UPLOAD_DIR = os.path.join(MEDIA_ROOT, UPLOAD_TO)


def get_image_data(url):
    """
    Obtains width, height and the dominant color from a given image.
    The definition of the most dominant color is open to interpretation.
    Here we simply select the most frequent color as the dominant color.
    """

    image = urllib.request.urlopen(url)
    image = Image.open(image).convert("RGB")
    width, height = image.size
    colors = image.getcolors(maxcolors=width * height)
    rgb = sorted(colors, key=lambda t: t[0])[-1][1]
    dominant = f"{rgb[0]:02x}{rgb[1]:02x}{rgb[2]:02x}"
    return width, height, dominant


def get_random_suffix(length=7):
    """
    Generates random suffixes for unique photo filenames.
    """

    choose_from = string.digits + string.ascii_letters
    return "_" + "".join(random.choices(choose_from, k=length))


def load_json(url):
    """
    Loads JSON data from an external API.
    """

    with urllib.request.urlopen(url) as response:
        return json.loads(response.read())


def pre_upload_photo(url):
    """
    Sanitizes filenames and prepares photos for upload.
    """

    photo = urllib.request.urlopen(url)
    image = Image.open(photo).convert("RGB")

    name = os.path.basename(url).split(".")[0]
    extension = image.format.lower() if image.format else "png"

    if os.path.exists(os.path.join(UPLOAD_DIR, f"{name}.{extension}")):
        name += get_random_suffix()

    name = f"{name}.{extension}"
    return name, extension, image


def insert_data(photos, database):
    """
    Adds photo records to the database and uploads the photos.
    """

    if not os.path.exists(database):
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), database)

    connection = sqlite3.connect(database)
    cursor = connection.cursor()

    for photo in photos:
        width, height, dominant = get_image_data(photo["url"])
        name, extension, image = pre_upload_photo(photo["url"])
        save_as = os.path.join(UPLOAD_TO, name)

        row = (
            photo["title"],
            photo["albumId"],
            width,
            height,
            dominant,
            save_as,
        )

        sql = """ INSERT INTO photomanager_photo(title,albumId,width,height,color,url)
                    VALUES(?,?,?,?,?,?) """

        cursor.execute(sql, row)
        connection.commit()

        # Database has been updated successfully, we can now upload the photo:
        image.save(os.path.join(UPLOAD_DIR, name), format=extension)

    connection.close()


def create_argument_parser():
    """
    Creates argument parser.
    """

    parser = argparse.ArgumentParser(description="Imports photos from an external API.")

    parser.add_argument(
        "--url",
        help="URL to an external API",
    )

    parser.add_argument(
        "--sqlite",
        help="Path to SQLite file (default: %(default)s)",
        default="db.sqlite3",
    )

    args = parser.parse_args()
    return args


def main():
    """
    Initiates the script.
    """

    parser = create_argument_parser()
    url = parser.url
    database = parser.sqlite

    print("[1/3] Connecting to an external API and parsing JSON...")

    try:
        data = load_json(url)

        # Make sure we always deal with a list, even when there's only one
        # object defined as Python's dict in the JSON file.
        if isinstance(data, dict):
            data = [data]

        print("[2/3] Downloading photos and adding records to the database...")

        insert_data(data, database)

    except json.decoder.JSONDecodeError:
        print(f"Error: URL '{url}' contains invalid JSON.")
    except urllib.error.HTTPError:
        print("Error: One or more external resouces could not be downloaded.")

    print(f"[3/3] Successfully added {len(data)} photos to the database.")


if __name__ == "__main__":
    main()
