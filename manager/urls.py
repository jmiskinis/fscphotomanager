"""
Defines the `urlpatterns` list which routes URLs to views.
"""

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers
from manager.api import views

router = routers.DefaultRouter()
router.register(r"api/v1/photos", views.PhotoViewSet)


urlpatterns = [
    path(
        "api/v1/photos/import/",
        views.DataImport.as_view({"post": "create", "get": "list"}),
    ),
    path("", include(router.urls)),
    path("admin/", admin.site.urls),
    path("api-auth/", include("rest_framework.urls", namespace="rest_framework")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
