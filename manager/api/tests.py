"""
Test cases for our REST API.
"""

import os
import errno
import json
import urllib.request
from rest_framework.test import APITestCase
from rest_framework import status
from manager.settings import BASE_DIR, MEDIA_ROOT
from manager.api.models import Photo
from manager.api.utils.images import create_in_memory_uploaded_file

# pylint: disable=no-member
UPLOAD_DIR = os.path.join(MEDIA_ROOT, Photo.url.field.upload_to)
TEST_JSON_FILE = os.path.join(BASE_DIR, "sample_data.json")
TEST_EXTERNAL_API = (
    "https://gitlab.com/jmiskinis/fscphotomanager/-/raw/main/uploads/sample_data/api"
)

TEST_IMAGE_URL = (
    "https://gitlab.com/jmiskinis/fscphotomanager/-/raw/main/uploads/sample_data/92c952"
)


class PhotoCreateTestCase(APITestCase):
    """
    Test case for creation of a `Photo` using the API.
    """

    # pylint: disable=missing-function-docstring
    def test_photo_create(self):
        # pylint: disable=no-member
        initial_photo_count = Photo.objects.count()
        in_memory = create_in_memory_uploaded_file(TEST_IMAGE_URL)

        photo = {
            "title": "Test Photo",
            "albumId": 5,
            "url": in_memory,
        }

        response = self.client.post("/api/v1/photos/", photo)

        if response.status_code != status.HTTP_201_CREATED:
            print(response.data)

        self.assertEqual(Photo.objects.count(), initial_photo_count + 1)

        for attr, expected_value in photo.items():
            # Photo might have been renamed after upload (e.g. duplicate file),
            # therefore we need this custom check:
            if attr == "url":
                uploaded_name = response.data[attr].rsplit("/")[-1]
                path = os.path.join(UPLOAD_DIR, uploaded_name)

                if os.path.exists(path):
                    os.remove(path)
                    continue

                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), path)

            self.assertEqual(response.data[attr], expected_value)

        # Check dynamically created fields:
        self.assertEqual(response.data["width"], 600)
        self.assertEqual(response.data["height"], 600)
        self.assertEqual(response.data["color"], "92c952")


class PhotoDestroyTestCase(APITestCase):
    """
    Test case for removal of a `Photo` using the API.
    """

    # pylint: disable=missing-function-docstring
    def setUp(self):
        in_memory = create_in_memory_uploaded_file(TEST_IMAGE_URL)

        photo = {
            "title": "Test Photo",
            "albumId": 5,
            "url": in_memory,
        }

        Photo.objects.create(**photo)

    # pylint: disable=missing-function-docstring
    def test_delete_photo(self):
        initial_product_count = Photo.objects.count()
        photo_id = Photo.objects.first().id
        self.client.delete(f"/api/v1/photos/{photo_id}/")
        self.assertEqual(Photo.objects.count(), initial_product_count - 1)
        self.assertRaises(Photo.DoesNotExist, Photo.objects.get, id=photo_id)


class PhotoListTestCase(APITestCase):
    """
    Test case for listing of `Photo`s using the API.
    """

    # pylint: disable=missing-function-docstring
    def setUp(self):
        in_memory = create_in_memory_uploaded_file(TEST_IMAGE_URL)

        photo = {
            "title": "Test Photo",
            "albumId": 5,
            "url": in_memory,
        }

        response = self.client.post("/api/v1/photos/", photo)
        self.photo_id = response.data["id"]

    # pylint: disable=missing-function-docstring
    def test_list_photos(self):
        photos_count = Photo.objects.count()
        response = self.client.get("/api/v1/photos/")
        self.assertEqual(len(response.data), photos_count)
        self.client.delete(f"/api/v1/photos/{self.photo_id}/")


class PhotoUpdateTestCase(APITestCase):
    """
    Test case for updating the `Photo` from the API.
    """

    # pylint: disable=missing-function-docstring
    def setUp(self):
        in_memory = create_in_memory_uploaded_file(TEST_IMAGE_URL)

        photo = {
            "title": "Test Photo",
            "albumId": 5,
            "url": in_memory,
        }

        self.client.post("/api/v1/photos/", photo)

    # pylint: disable=missing-function-docstring
    def test_update_photo(self):
        in_memory = create_in_memory_uploaded_file(TEST_IMAGE_URL)
        photo = Photo.objects.first()

        response = self.client.patch(
            f"/api/v1/photos/{photo.id}/",
            {
                "title": "New Title",
                "albumId": 0,
                "url": in_memory,
            },
            format="multipart",
        )

        updated = Photo.objects.get(id=photo.id)
        self.assertEqual(updated.title, "New Title")
        self.assertEqual(updated.albumId, 0)
        uploaded_name = response.data["url"].rsplit("/")[-1]
        path = os.path.join(UPLOAD_DIR, uploaded_name)

        if os.path.exists(path):
            os.remove(path)
        else:
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), path)


class PhotoImportFromJSONFileTestCase(APITestCase):
    """
    Test case for importing photos from a JSON file using API.
    """

    # pylint: disable=missing-function-docstring
    def test_import_photos_from_json_file(self):
        photo_count = Photo.objects.count()

        with open(TEST_JSON_FILE, encoding="utf_8") as content:
            data = json.load(content)

        if isinstance(data, dict):
            data = [data]

        json_object_count = len(data)

        with open(TEST_JSON_FILE, "rb") as json_data:
            self.client.post(
                "/api/v1/photos/import/",
                {"json_file_import": json_data},
                format="multipart",
            )

        self.assertEqual(Photo.objects.count(), photo_count + json_object_count)

        for photo in Photo.objects.all():
            self.client.delete(f"/api/v1/photos/{photo.id}/")


class PhotoImportFromExtAPITestCase(APITestCase):
    """
    Test case for importing photos from an external API using API.
    """

    # pylint: disable=missing-function-docstring
    def test_import_photos_from_ext_api(self):
        photo_count = Photo.objects.count()

        with urllib.request.urlopen(TEST_EXTERNAL_API) as response:
            data = json.loads(response.read())

        if isinstance(data, dict):
            data = [data]

        json_object_count = len(data)

        self.client.post(
            "/api/v1/photos/import/",
            {"ext_api_import": TEST_EXTERNAL_API},
        )

        self.assertEqual(Photo.objects.count(), photo_count + json_object_count)

        for photo in Photo.objects.all():
            self.client.delete(f"/api/v1/photos/{photo.id}/")
