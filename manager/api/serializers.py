"""
Defines the serializer to be used in the project. Serializers converts complex
data, such as querysets or model instances, into native Python datatypes.
"""

from rest_framework import serializers
from manager.api.utils.images import get_dominant_color
from manager.api.models import Photo, DataImport


class PhotoSerializer(serializers.ModelSerializer):
    """
    Uses `ModelSerializer` which allows to automatically create a `Serializer`
    class with fields that correspond to our model fields.
    """

    class Meta:
        """
        Describes the metadata to be used in the serializer.
        """

        model = Photo
        fields = "__all__"

    def get_fields(self, *args, **kwargs):
        """
        Updating photos may not involve uploading new ones (e.g. we may only
        change the title), therefore we set the `url` field as optional for
        requests other than `POST`.
        """

        fields = super(PhotoSerializer, self).get_fields(*args, **kwargs)
        request = self.context.get("request", None)

        if request and getattr(request, "method", None) != "POST":
            fields["url"].required = False

        return fields

    def create(self, validated_data):
        """
        Ensures that dynamic data fields, e.g. dominant color of a photo, are
        calculated before the creation of a `Photo` instance.
        """
        validated_data["color"] = get_dominant_color(validated_data["url"].file)
        # pylint: disable=no-member
        photo = Photo.objects.create(**validated_data)
        return photo

    def update(self, instance, validated_data):
        """
        Deletes the old photo from the system whenever user updates a `Photo`
        instance with a new photo. Also, calculates the dominant color of the
        new photo.
        """

        instance.title = validated_data.get("title", instance.title)
        instance.albumId = validated_data.get("albumId", instance.albumId)

        if "url" in validated_data:
            instance.color = get_dominant_color(validated_data["url"].file)
            instance.url.delete(save=False)

        instance.width = validated_data.get("width", instance.width)
        instance.height = validated_data.get("height", instance.height)
        instance.url = validated_data.get("url", instance.url)
        instance.save()
        return instance


class DataImportSerializer(serializers.ModelSerializer):
    """
    Uses `ModelSerializer` which allows to automatically create a `Serializer`
    class with fields that correspond to our model fields.
    """

    class Meta:
        """
        Describes the metadata to be used in the serializer.
        """

        model = DataImport
        fields = ["json_file_import", "ext_api_import"]
