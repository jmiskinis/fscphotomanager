"""
Combines the logic for a set of related views in single `ViewSet` classes.
"""

import json
import logging
import urllib.error
import urllib.request
from rest_framework.exceptions import ValidationError
from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets
from manager.api.models import Photo
from manager.api.serializers import PhotoSerializer, DataImportSerializer
from manager.api.utils.images import create_in_memory_uploaded_file


class PhotoViewSet(viewsets.ModelViewSet):
    """
    Lists of all photos in the databse.
    """

    # pylint: disable=no-member
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer


class DataImport(viewsets.ModelViewSet):
    """
    Allows importing data from JSON files and external APIs.
    """

    serializer_class = DataImportSerializer

    def create(self, request, *args, **kwargs):
        try:
            json_api_import = request.data.get("json_file_import")
            ext_api_import = request.data.get("ext_api_import")

            if json_api_import:
                json_api_import = json.load(json_api_import)

            if ext_api_import:
                with urllib.request.urlopen(ext_api_import) as response:
                    ext_api_import = json.loads(response.read())

            # In case both import methods are used, ensure all data is
            # serialized and validated before adding it to the database:
            if ext_api_import and json_api_import:
                ext_api_serializer = self.serialize_and_validate(ext_api_import)
                json_file_serializer = self.serialize_and_validate(json_api_import)
                ext_api_serializer.save()
                json_file_serializer.save()

            elif ext_api_import:
                ext_api_serializer = self.serialize_and_validate(ext_api_import)
                ext_api_serializer.save()

            elif json_api_import:
                json_file_serializer = self.serialize_and_validate(json_api_import)
                json_file_serializer.save()

        except json.decoder.JSONDecodeError:
            return Response(
                {
                    "code": status.HTTP_400_BAD_REQUEST,
                    "message": "Imported data is not a valid JSON.",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        except urllib.error.HTTPError:
            return Response(
                {
                    "code": status.HTTP_404_NOT_FOUND,
                    "message": "One or more external resources could not be downloaded.",
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        except ValidationError as errors:
            return Response(
                {
                    "code": status.HTTP_400_BAD_REQUEST,
                    "message": str(errors),
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        # pylint: disable=broad-except
        except Exception as error:
            # This is discouraged broad exception handling, but here we use a
            # good practice of logging the exception:
            logging.exception(error)

            return Response(
                {
                    "code": status.HTTP_400_BAD_REQUEST,
                    "message": "There has been an error. Please contact service administrators.",
                },
                status=status.HTTP_400_BAD_REQUEST,
            )
        else:
            return Response(
                {
                    "code": status.HTTP_200_OK,
                    "message": "Photos were successfully imported to the databse.",
                },
                status=status.HTTP_200_OK,
            )

    def list(self, request, *args, **kwargs):
        """
        Prints out a description of the GET method endpoint. This endpoint is
        not really necessary but nonetheless it allows convenient data upload
        via Django REST Framework's frontend (useful during testing).
        """
        return Response(
            {
                "code": status.HTTP_200_OK,
                "message": "Use this endpoint to import data from JSON files or external APIs.",
            }
        )

    def serialize_and_validate(self, data):
        """
        Serializes and validates data.
        """

        # Make sure we always deal with a list, even when there's only one
        # object defined as Python's dict in the JSON file.
        if isinstance(data, dict):
            data = [data]

        objects = []

        for photo in data:
            objects.append(
                {
                    "title": photo["title"],
                    "albumId": photo["albumId"],
                    "url": create_in_memory_uploaded_file(photo["url"]),
                }
            )

        serializer = PhotoSerializer(data=objects, many=True)

        if not serializer.is_valid():
            raise ValidationError(serializer.errors)

        return serializer
