"""
Uses a Django model to describe the data to be used in the project.
"""

from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_delete


class Photo(models.Model):
    """
    Describes essential data fields and behaviors for a given model.
    """

    title = models.CharField(max_length=200)
    albumId = models.PositiveIntegerField()
    width = models.PositiveIntegerField(editable=False)
    height = models.PositiveIntegerField(editable=False)
    color = models.CharField(max_length=6, editable=False)

    url = models.ImageField(
        null=False,
        upload_to="photos",
        verbose_name="Photo",
        width_field="width",
        height_field="height",
    )

    def __str__(self):
        """
        Makes sure class objects are represented as titles for
        better readability.
        """
        return f"{self.title}"


@receiver(post_delete, sender=Photo)
# pylint: disable=unused-argument
def post_save_image(sender, instance, *args, **kwargs):
    """
    By default, removing a `Photo` instance from the database
    leaves behind the associated image file. This ensures that
    the file is removed as well.
    """
    instance.url.delete(save=False)


class DataImport(models.Model):
    """
    Describes essential data fields and behaviors for a given model.
    """

    json_file_import = models.FileField(
        blank=True, verbose_name="Import from a JSON file"
    )

    ext_api_import = models.URLField(
        blank=True, verbose_name="Import from external API"
    )
