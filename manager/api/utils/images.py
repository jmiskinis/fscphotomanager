"""
Contains utility functions to be used in the project.
"""

import sys
import os.path
from io import BytesIO
import urllib.request
from PIL import Image
from django.core.files.uploadedfile import InMemoryUploadedFile


def get_dominant_color(file):
    """
    Obtains the dominant color from a given image. The definition
    of the most dominant color is open to interpretation. Here we
    simply select the most frequent color as the dominant color.
    """

    img = Image.open(file).convert("RGB")
    width, height = img.size
    colors = img.getcolors(maxcolors=width * height)
    _, rgb = sorted(colors, key=lambda t: t[0])[-1]
    return f"{rgb[0]:02x}{rgb[1]:02x}{rgb[2]:02x}"


def create_in_memory_uploaded_file(url):
    """
    Creates `InMemoryUploadedFile` - object representing a file uploaded into
    memory and required during Django file upload operations.
    """

    image = urllib.request.urlopen(url)
    image_io = BytesIO()
    image = Image.open(image).convert("RGB")

    name = os.path.basename(url).split(".")[0]
    extension = image.format.lower() if image.format else "png"

    image.save(image_io, format=extension)
    image_io.seek(0)

    return InMemoryUploadedFile(
        image_io,
        "photo",
        f"{name}.{extension}",
        "image/png" if extension == "png" else image.get_format_mimetype(),
        sys.getsizeof(image_io),
        None,
    )
