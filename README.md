# FSC Photo Manager

This is a simple photo manager based on [Django REST Framework](https://www.django-rest-framework.org/).
## Installation

### Required applications
* Python 3.10
* pip 22.3
### Required Python packages (also listed in [requirements.txt](requirements.txt))
```
Django==4.1.2
djangorestframework==3.14.0
Pillow==9.2.0
```

### Steps   
```sh
git clone https://gitlab.com/jmiskinis/fscphotomanager
cd fscphotomanager
python3 -m venv env
source env/bin/activate  # On Windows use `env\Scripts\activate`
pip install -r requirements.txt
python manage.py runserver
```

## Using the API

Below is the list of `curl` commands required to send requests to the API. Alternatively, you can use [Django REST Framework frontend](http://127.0.0.1:8000/api/v1/photos/) to perform the same operations.

### List

```sh
curl http://127.0.0.1:8000/api/v1/photos/
```
### Create

```sh
curl -X POST http://127.0.0.1:8000/api/v1/photos/ -F title="My Photo" -F albumId=1 -F url=@"/home/joasis/Photos/cat.jpeg"
```

### Update and partial update

```sh
curl -X PUT http://127.0.0.1:8000/api/v1/photos/1/ -F title="My New Ttle" -F albumId=1 -F url=@"/home/joasis/Photos/one_more_cat.jpeg"
curl -X PATCH http://127.0.0.1:8000/api/v1/photos/1/ -d title="My New Title"
```

### Delete
```sh
curl -X DELETE http://127.0.0.1:8000/api/v1/photos/1/
```

### Import from a JSON file
```sh
curl -X POST http://127.0.0.1:8000/api/v1/photos/import/ -F json_file_import=@"/home/joasis/Downloads/sample_data.json"
```

### Import from an external API
```sh
curl -X POST http://127.0.0.1:8000/api/v1/photos/import/ -d ext_api_import="https://gitlab.com/jmiskinis/fscphotomanager/-/raw/main/uploads/sample_data/api"
```

Note that you can combine the above two into a single request.

## Testing

This API includes a testing framework for the request methods listed above. Specifically, 6 classes were implemented:

```
PhotoListTestCase
PhotoCreateTestCase
PhotoDestroyTestCase
PhotoUpdateTestCase
PhotoImportFromJSONFileTestCase
PhotoImportFromExtAPITestCase
```

To perform the tests, navigate to project's root folder and run:

```sh
python manage.py test
```

## CLI tools

Both CLI tools, `ext_api_importer.py` and `json_file_importer.py`, can be accessed from project's root folder. They rely on some relative imports for Django REST Framework, namely the `upload_to=` property, defined in API's models. For this reason, these two files should not be moved outside the root folder. There might be a way to provide require upload paths as a parameter but I haven't figured out how yet.

Run the tools with an `-h` flag to view available parameters:

```sh
python json_file_import.py -h
python ext_api_import.py -h
```

If you'd like to test these tools using project's sample resources, run them like so:

```sh
python json_file_import.py --json sample_data.json
python ext_api_import.py --url "https://gitlab.com/jmiskinis/fscphotomanager/-/raw/main/uploads/sample_data/api"
```

## A note on [jsonplaceholder.typicode.com/photos](https://jsonplaceholder.typicode.com/photos)

FSC Photo Manager can parse the text data provided by [jsonplaceholder.typicode.com/photos/](https://jsonplaceholder.typicode.com/photos). However, it cannot automatically download any images becauses they are stored in servers protected with Cloudflare's security services against online attacks. Currently, it's not in my interest to try and bypass them. As an alternative, feel free to use this sample api which mirrors the one we had to work with:
```
https://gitlab.com/jmiskinis/fscphotomanager/-/raw/main/uploads/sample_data/api
```

## Some ideas on how to improve this API

One of my goals while working on this task, was to not overcomplicate things. That's why I used the suggested Django REST Framework. Having said that, here's a list of ideas on how we could further improve this API:
1. Any REST API in production should use authentication for methods that modify the database. Django REST Framework has one implemented but I didn't use it to simplify testing.
2. If there's a demand, REST APIs should use query parameters to filter data.
3. REST APIs with large databases should use pagination. I have not, to simplify testing, although it's trivial with Django REST Framework.
4. I haven't look into how caching is implemented in Django REST Framework. It's possible that we need to take care of it manually, e.g. clean it when we remove instances from the database. 
5. We should add more constraints to data we parse to the API. For example, file size limits. I'm not sure why it hasn't been implemented in Django yet.
6. We should look into more carefully at how we scrape data from external APIs. Methods of minimizing the risk of being denied access should be considered.
7. In this project, classes, functions and methods are only partially documented. Python's docstring should also include definitions of parameters and return statements.
8. We should also write unit test for the two project's CLI tools.
9. Implement project's testing framework into Github's Actions or GitLab's CI/CD.